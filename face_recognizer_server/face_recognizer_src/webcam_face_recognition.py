import face_recognition
import cv2
import pickle
import os
import numpy as np
from pathlib import Path
from itertools import compress
from face_recognition.api import _raw_face_landmarks, face_encoder
from face_recognizer_src.settings import BASE_PATH, CAFFE_PROTO_TXT_PATH, CAFFE_MODEL_PATH, PERSONS_VIDEO_PATH, \
    PERSONS_PHOTO_PATH


class DataWorker:
    @staticmethod
    def create_faces_dataset(face_encodings, full_path=os.path.join(BASE_PATH, 'datasets/dataset_faces.dat')):
        DataWorker._save_face_encodings(face_encodings, full_path)
        return face_encodings

    @staticmethod
    def load_face_encodings(full_path=os.path.join(BASE_PATH, 'datasets/dataset_faces.dat')):
        with open(full_path, 'rb') as f:
            all_face_encodings = pickle.load(f)
        return all_face_encodings

    @staticmethod
    def get_face_encodings_from_local_images(path=PERSONS_PHOTO_PATH):
        folders = os.listdir(path)

        all_face_encodings = {}
        exception_no_face_counter = 0
        exception_memory_counter = 0

        # Load a sample picture and learn how to recognize it.
        for directory in folders:
            path_to_folder_with_images = '{}/{}'.format(path, directory)
            images_in_folder = os.listdir(path_to_folder_with_images)
            for img_index, image in enumerate(images_in_folder):
                directory_name_with_img_index = '{}_img{}'.format(directory.capitalize(), img_index)
                path_to_singe_image = '{}/{}'.format(path_to_folder_with_images, image)
                print(path_to_folder_with_images, image)
                image = face_recognition.load_image_file(path_to_singe_image)
                try:
                    all_face_encodings[directory_name_with_img_index] = \
                        custom_face_encodings_with_large_model_for_landmarks(
                            image, get_face_locations_with_all_models(image))[0]
                except MemoryError:
                    print('EXCEPTION MemoryError! ', path_to_singe_image)
                    exception_memory_counter += 1
                except IndexError:
                    # If photo doesn't have faces or models can't detect face
                    print('EXCEPTION IndexError! ', path_to_singe_image)
                    exception_no_face_counter += 1

            print('folder: ', path_to_folder_with_images, 'count of photos without faces: ', exception_no_face_counter)
            print('folder: ', path_to_folder_with_images, 'memory exceptions: ', exception_memory_counter)
            exception_no_face_counter = 0

        return all_face_encodings

    @staticmethod
    def get_face_encodings_from_local_video(path=PERSONS_VIDEO_PATH):
        video_dict = DataWorker.get_dict_of_video_and_user_id_in_folder(path)
        return DataWorker.get_face_encodings_from_video_dict(video_dict)

    @staticmethod
    def get_face_encodings_from_video_dict(video_dict, path_to_dataset_for_updating=None, face_encodings_data=None):
        """
        Given an dictionary, with path to training video and user id, return the encoded faces for each frame of video

        :param video_dict: Dictionary, with path to training video and user id
        :param path_to_dataset_for_updating: Path to dataset from which we will take index for updating data
        :param face_encodings_data: face encodings data
        :return: The encoded faces for each frame of video
        """

        all_face_encodings = {}
        frame_counter = 0
        for video_file, user_id in video_dict.items():
            print("processing video:", video_file)
            video_capture = cv2.VideoCapture(video_file)

            last_position_in_dataset = 0

            if path_to_dataset_for_updating or face_encodings_data:
                last_position_in_dataset = DataWorker.get_last_position_in_dataset(path_to_dataset_for_updating,
                                                                                   'video', user_id,
                                                                                   face_encodings_data)

            rotation_degree = FrameProcessor.get_rotation_degree_for_video(video_file)
            while True:
                ret, frame = video_capture.read()
                if not ret:
                    break

                if rotation_degree:
                    frame = FrameProcessor.rotate_image(frame, rotation_degree)

                face_locations = get_face_locations_with_all_models(frame)
                if face_locations:
                    directory_name_with_frame_index = '{}_frame{}' \
                        .format(user_id, frame_counter + last_position_in_dataset + 1)
                    print(directory_name_with_frame_index)
                    all_face_encodings[directory_name_with_frame_index] = \
                        custom_face_encodings_with_large_model_for_landmarks(frame, face_locations, num_jitters=1)[0]
                    frame_counter += 1

            # Release handle to the webcam
            video_capture.release()
        return all_face_encodings

    @staticmethod
    def get_face_encodings_from_image_dict(image_dict, path_to_dataset_for_updating=None, face_encodings_data=None):
        """
        Given an dictionary, with path to training image and user id, return the dict with
        index started from last index in`dataset_for_updating` and encoded faces for each image.


        :param image_dict:  Dictionary , with path to training image and user id
        :param path_to_dataset_for_updating: Path to dataset from which we will take index for updating data
        :param face_encodings_data: face encodings data
        :return: The encoded faces for each image
        """

        all_face_encodings = {}
        exception_no_face_counter = 0
        exception_memory_counter = 0

        last_position_in_dataset = 0

        for index, (path_to_singe_image, user_id) in enumerate(image_dict.items()):
            print("processing image:", path_to_singe_image)
            image = face_recognition.load_image_file(path_to_singe_image)

            if path_to_dataset_for_updating or face_encodings_data:
                last_position_in_dataset = DataWorker.get_last_position_in_dataset(path_to_dataset_for_updating,
                                                                                   'image', user_id,
                                                                                   face_encodings_data)

            user_id_with_img_index = '{}_img{}'.format(user_id, index + last_position_in_dataset + 1)
            print(user_id_with_img_index)
            try:
                all_face_encodings[user_id_with_img_index] = \
                    custom_face_encodings_with_large_model_for_landmarks(
                        image, get_face_locations_with_all_models(image))[0]
            except MemoryError:
                print('EXCEPTION MemoryError! ', path_to_singe_image)
                exception_memory_counter += 1
            except IndexError:
                # If photo doesn't have faces or models can't detect face
                print('EXCEPTION IndexError! ', path_to_singe_image)
                exception_no_face_counter += 1
        return all_face_encodings

    @staticmethod
    def get_dict_of_video_and_user_id_in_folder(path=PERSONS_VIDEO_PATH):
        video_to_user = {}
        folders = os.listdir(path)
        for user_id in folders:
            path_to_user_video = '{}/{}'.format(path, user_id)
            if not os.path.isdir(path_to_user_video):
                continue
            video_in_folder = os.listdir(path_to_user_video)
            for file in video_in_folder:
                path_to_video = '{}/{}/{}'.format(path, user_id, file)
                video_to_user[path_to_video] = user_id
        return video_to_user

    @staticmethod
    def _save_face_encodings(all_face_encodings, full_path):
        with open(full_path, 'wb') as f:
            pickle.dump(all_face_encodings, f)

    @staticmethod
    def get_last_position_in_dataset(dataset_path, data_type, user_id, face_encodings_data):
        """
        Function for get last index of encoded face from photo or video frame

        :param dataset_path: path to dataset file with face encodings, using if face_encodings is None
        :param data_type: Type of encoded data with person face
        :param user_id: Id of person
        :param face_encodings_data: face encodings data
        :return:
        """
        if face_encodings_data:
            all_face_encodings = face_encodings_data
        else:
            all_face_encodings = DataWorker.load_face_encodings(dataset_path)

        if data_type == 'image':
            delimiter = '{}_img'.format(user_id)
        elif data_type == 'video':
            delimiter = '{}_frame'.format(user_id)
        else:
            return 0

        all_keys_by_user_id = [int(key.split(delimiter)[1]) for key, value in all_face_encodings.items() if
                               delimiter in key.lower()]

        if all_keys_by_user_id:
            return max(all_keys_by_user_id)
        else:
            return 0


class FaceRecognizer:
    def __init__(self, faces_dataset, tolerance=0.31):
        self.faces_dataset = faces_dataset
        self.tolerance = tolerance

    def get_face_names_and_locations(self, frame, confidence=0.7):
        # Caffe face detection model
        face_locations = get_face_locations_by_caffe_model(frame, confidence)

        face_names_in_frame = []
        faces_from_dataset = []
        faces_list_names_from_dataset = []

        for item in self.faces_dataset:
            faces_from_dataset.append(self.faces_dataset[item])
            faces_list_names_from_dataset.append(item)

        for face_encoding in custom_face_encodings_with_large_model_for_landmarks(frame, face_locations):
            name = self.get_name_of_person_by_image(face_encoding,
                                                    faces_from_dataset,
                                                    faces_list_names_from_dataset)
            face_names_in_frame.append(name)
        print("Stuff ids: ", face_names_in_frame)
        return face_names_in_frame, face_locations

    def get_name_of_person_by_image(self, frame, encoded_face_list, faces_list_names):
        # See if the face is a match for the known face(s)
        match = face_recognition.compare_faces(encoded_face_list, frame, self.tolerance)
        # Get indexes of recognized faces
        recognized_idx = list(compress(range(len(match)), match))
        return FaceRecognizer.get_name_of_recognized_person(faces_list_names, recognized_idx)

    @staticmethod
    def get_name_of_recognized_person(faces_list, recognized_idx):
        name = "Unknown"
        if len(faces_list) > 0:
            for i in recognized_idx:
                name = "_".join(faces_list[i].split("_")[:-1])
            return name
        else:
            return name


class FrameAdapter:
    def __init__(self, resize_coef):
        self.resize_coef = resize_coef

    def process_frame(self, frame):
        if not isinstance(frame, np.ndarray):
            frame = cv2.imread(frame)

        # Resize frame of video to `resize_coef` size for faster face recognition processing
        return cv2.resize(frame, (0, 0), fx=self.resize_coef, fy=self.resize_coef)


class FrameProcessor:
    def __init__(self, frame_adapter=FrameAdapter(0.5)):
        self.resize_coef = frame_adapter.resize_coef

    def draw_rectangles_on_frame(self, frame, face_locations, face_names, frame_name):
        # Display the results
        for (top, right, bottom, left), name in zip(face_locations, face_names):
            # Scale back up face locations since the frame we detected in was scaled to 1/4 size
            top *= int(1 / self.resize_coef)
            right *= int(1 / self.resize_coef)
            bottom *= int(1 / self.resize_coef)
            left *= int(1 / self.resize_coef)

            # Draw a box around the face
            cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

            # Draw a label with a name below the face
            cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

        # Display the resulting image
        cv2.imshow(frame_name, frame)

    @staticmethod
    def get_image_in_four_rotation(image):
        # grab the dimensions of the image and calculate the center
        # of the image
        (h, w) = image.shape[:2]
        center = (w / 2, h / 2)
        degree = 90

        four_rotation_of_frame = [
            cv2.warpAffine(image, cv2.getRotationMatrix2D(center, degree * i, 1.0), (w, h)) for i in range(0, 4)]
        return four_rotation_of_frame

    @staticmethod
    def get_rotation_degree_for_video(video):
        """
        1. Create dict with different rotations
        2. Read frames from video while video not ended or when we get 40 times of face detections for some orientation
        3. Return value of degree for rotation

        :param video: Video for checking orientation
        :return: Value of degree for rotation
        """
        print("Check orientation of video {}".format(video))
        rotation = {0: 0, 90: 0, 180: 0, 270: 0}

        video_capture = cv2.VideoCapture(video)
        while max(rotation.values()) < 40:
            ret, frame = video_capture.read()
            if not ret:
                break
            rotated_frames = FrameProcessor.get_image_in_four_rotation(frame)
            for index, rotated_frame in enumerate(rotated_frames):
                face_locations = get_face_locations_with_all_models(rotated_frame)
                if face_locations:
                    rotation[index * 90] = rotation[index * 90] + 1

        # Release handle to the webcam
        video_capture.release()

        if max(rotation) == 0:
            return None

        return max(rotation)

    @staticmethod
    def rotate_image(image, rotation_degree):
        (h, w) = image.shape[:2]
        center = (w / 2, h / 2)
        return cv2.warpAffine(image, cv2.getRotationMatrix2D(center, rotation_degree, 1.0), (w, h))


def custom_face_encodings_with_large_model_for_landmarks(face_image, known_face_locations=None, num_jitters=1):
    """
    Given an image, return the 128-dimension face encoding for each face in the image.

    :param face_image: The image that contains one or more faces
    :param known_face_locations: Optional - the bounding boxes of each face if you already know them.
    :param num_jitters: How many times to re-sample the face when calculating encoding. Higher is more accurate, but slower (i.e. 100 is 100x slower)
    :return: A list of 128-dimensional face encodings (one for each face in the image)
    """
    raw_landmarks = _raw_face_landmarks(face_image, known_face_locations, model="large")
    return [np.array(face_encoder.compute_face_descriptor(face_image, raw_landmark_set, num_jitters)) for
            raw_landmark_set in raw_landmarks]


def run(faces_dataset, camera_id, tolerance=0.31, confidence=0.9):
    """
    This is a running face recognition on live video from your webcam.
    Given a face dataset, camera id, return the live video output with recognitions

    1. Process each video frame in full resolution
    2. Only detect faces in every other frame of video.

    :param faces_dataset:
    :param camera_id:
    :param tolerance:
    :param confidence:
    :return:
    """
    resize_coef = 1

    face_recognizer = FaceRecognizer(faces_dataset, tolerance)
    frame_adapter = FrameAdapter(resize_coef)
    frame_processor = FrameProcessor(frame_adapter)

    # Get a reference to webcam #0 (the default one)
    video_capture = cv2.VideoCapture(camera_id)

    while True:
        # Grab a single frame of video
        ret, frame = video_capture.read()

        if ret is False:
            break

        small_frame = frame_adapter.process_frame(frame)

        # Find all the faces and face encodings in the current frame of video
        face_names, face_locations = face_recognizer.get_face_names_and_locations(small_frame, confidence=confidence)

        # continue
        frame_processor.draw_rectangles_on_frame(frame, face_locations, face_names, str(camera_id))

        # Hit 'q' on the keyboard to quit!
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # Release handle to the webcam
    video_capture.release()
    cv2.destroyAllWindows()


def get_face_locations_by_caffe_model(image, confidence=0.9):
    """
    Given an image, return the tuple of rect points (top, right, bottom, left) for each face in the image.
    Apply caffe face detection model to an image.

    :param image: The image that contains one or more faces
    :param confidence:
    :return: A list of tuples of rect points (top, right, bottom, left) for each face in the image.
    """

    args = {"prototxt": CAFFE_PROTO_TXT_PATH, "model": CAFFE_MODEL_PATH, "confidence": confidence}
    net = cv2.dnn.readNetFromCaffe(args["prototxt"], args["model"])
    (h, w) = image.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(image, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0))
    net.setInput(blob)
    detections = net.forward()

    face_locations = []
    for i in range(0, detections.shape[2]):
        confidence = detections[0, 0, i, 2]
        if confidence < args["confidence"]:
            continue
        box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
        (left, top, right, bottom) = box.astype("int")

        face_location_tuple = (top, right, bottom, left)
        face_locations.append(face_location_tuple)

    return face_locations


def default_cnn(image):
    """
    Given an image, return the tuple of rect points (top, right, bottom, left) for each face in the image.
    Apply cnn face detection model to an image.

    :param image: The image that contains one or more faces
    :return: A list of tuples of rect points (top, right, bottom, left) for each face in the image.
    """

    return face_recognition.face_locations(image, number_of_times_to_upsample=1, model='cnn')


def get_face_locations_with_all_models(image):
    """
    Given an image, return the tuple of rect points (top, right, bottom, left) for each face in the image.
    Apply hog, cnn, caffe face detection models to an image.

    :param image: The image that contains one or more faces
    :return: A list of tuples of rect points (top, right, bottom, left) for each face in the image.
    """

    models_functions = [
        get_face_locations_by_caffe_model,
        face_recognition.face_locations,
        default_cnn
    ]
    for detection_func in models_functions:
        encodings = detection_func(image)
        if len(encodings) > 0:
            return encodings
    return []


def main(camera_id=0):
    dataset_path = os.path.join(BASE_PATH, 'datasets/dataset_faces.dat')
    # Save or load trained model if exists
    if Path(dataset_path).is_file():
        run(DataWorker.load_face_encodings(dataset_path), camera_id, tolerance=0.31, confidence=0.9)
    else:
        face_encodings = DataWorker.get_face_encodings_from_local_images()
        face_encodings.update(DataWorker.get_face_encodings_from_local_video())
        run(DataWorker.create_faces_dataset(face_encodings, dataset_path), camera_id, tolerance=0.31, confidence=0.9)


if __name__ == '__main__':
    main()
