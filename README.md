##BUILD##

*For production:* `docker-compose -f docker-compose-prod.yml build`

*For local:* `docker-compose build`

##RUN##

*For production:* `docker-compose -f docker-compose-prod.yml up`

*For local:* `docker-compose up`


##Deploying in venv:##

*dlib installation:* 

1) apt-get install build-essential cmake pkg-config libx11-dev libatlas-base-dev libgtk-3-dev libboost-python-dev

*Additional dependencies when install on Raspberry:*

1) apt-get install libopenjp2-7 libwebp-dev libjasper-dev libIlmbase-dev libopenexr-dev libgstreamer1.0-dev libavcodec-dev libavformat-dev libswscale-dev libqt4-dev

*Install pip dependencies:*

pip3 install -r requirements.txt